# When to Use Each Access Modifier

**Public** methods should be used to expose an API to your users/consumers.
**Private** methods should be internal functionality that you do not want to
expose to the public. We'll see many examples of how to use all the access
modifiers throughout this course.
