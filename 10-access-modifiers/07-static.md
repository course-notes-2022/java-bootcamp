# `static`

The `static` keyword indicates that the attribute/method belongs to the
**class**, not the **instance of the class**.

To refer to the `static` attr/method, you prefix it with the **class name**:

```java

public class Foo {
    //...

    static String foo = bar
}


public class Bar {

    public static printFoo() {
        System.out.println(Foo.foo);
    }
}

``
```
