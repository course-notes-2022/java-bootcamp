# `public` Access Modifier

The `public` access modifier means that the class/method/attribute/constructor
can be accessed by **any other class**:

```java

public class Main {
    public static String foo = "bar"; // available everywhere

    public static void main(String[] args) {
        //...
        String foo = bar; // local to the main method only
    }
}
```
