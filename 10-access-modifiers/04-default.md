# `default` Access Modifier

The `default` access modifier means that the constructor/class/method/attr is
accessible only by classes in the **same package**. You declare default access
by **not specifying any access modifier**.
