# Recap

| Modifier    | Accessible From         |
| ----------- | ----------------------- |
| `public`    | anywhere                |
| `private`   | owning class only       |
| `default`   | classes in same package |
| `protected` | classes in same package |
