# `private` Access Modifier

The `private` access modifier restricts access to the
attribute/method/class/constructor to the **class that owns it**:

```java

public class Main {
    private static String foo = "bar"

    public static void main() {
        System.out.println(foo);
    }
}

//...
public class Bar {
    public static void printFoo() {
        System.out.println(Main.foo); // NO
    }
}
```
