# String API Docs

The Java API docs describe the specification for the `String` class, including
the fields and methods associated with `String`.

[See the API docs for the `String` class here](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html).
