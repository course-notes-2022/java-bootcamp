# Numeric Literals with Underscore

In Java, commas can be replaced with **underscores** when defining numeric
values:

```java
package com.jfarrowdev;

public class Main {

    public static void main(String[] args) {
        int amount = 1_000_000_000;
        double newAmount = 1_000_000_000.01;

        System.out.println(amount);
    }
}
```
