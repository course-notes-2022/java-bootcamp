# Pass by Value: Primitives

Primitives in Java are **pass by value**:

```java
public class Main {
    public static void main(String[] args) {

        int age = 21;
        int ageCopy = age; // ageCopy receives a COPY of the VALUE stored in age
    }
}

```

If you later change the value of `age`, the value of `ageCopy` remains
**unaffected**.

![pass by value](./screenshots/pass-by-value.png)
