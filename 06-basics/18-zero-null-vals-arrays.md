# Null Values in Arrays

Note that the **default** for uninitialized values in arrays is `0` for
primitives. For reference types, it's `null`:

```java

public class Main {
    public static void main(String[] args) {
        // Arrays
        int[] numbers = new int[3];

        numbers[1] = 22

        System.out.println(Arrays.toString(numbers)); // [0, 22, 0]

       String[] names = new String[3];
       names[1] = "Jamilah";

       System.out.println(Arrays.toString(numbers)); // [null, "Jamilah", null]

    }
}

```
