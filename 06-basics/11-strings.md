# Strings

**Strings** are sequences of characters:

```java

    String brand = "AmigosCode";
```

Any character can be added between the `""`. You **must** use `""` for strings
in Java. Note that you can use the `+` operator to **concatenate** strings.
