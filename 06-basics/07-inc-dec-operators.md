# Increment/Decrement Operators

We can use the `++`/`--` operators to **increment/decrement** a value by one,
respectively:

```java
package com.jfarrowdev;

public class Main {

    public static void main(String[] args) {
        int amt = 0;
        amt++;
        System.out.println(amt); // 1
        amt--;
        System.out.println(amt) // 0
    }
}

```
