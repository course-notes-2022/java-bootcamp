# Arrays

An **array** is a collection of values we can store inside one variable.

```java

public class Main {
    public static void main(String[] args) {
        // Arrays
        int[] numbers = new int[3]; // specify the size of the array at init

        numbers[0] = 22; // Add items to the array
        numbers[1] = 9;
        numbers[2] = -1;

        System.out.println(Arrays.toString(numbers)); // [22, 9, -1]

        System.out.println(numbers.length); // 3

        // Init an array with elements inside
        int[] nums2 = {0, 5, 99, -1, 12345}
    }
}

```

Note that once we define the **size** of an array, we **cannot increase it**.
