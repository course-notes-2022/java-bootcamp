# Comments

There are two types of comments in Java:

- **Single-line** comments: Denoted by a `//`
- **Multi-line** comments: Denoted by `/* */` Comments do **not** get executed.
