# Arithmetic Operators

Java has **arithmetic operators** to allow us to perform common mathematical
operations:

| Operator | Operation                       |
| -------- | ------------------------------- |
| `+`      | Addition                        |
| `-`      | Subtraction                     |
| `*`      | Multiplication                  |
| `/`      | Division                        |
| `%`      | Modulus (remainder of division) |

The `PEMDAS` order of operations always applies.
