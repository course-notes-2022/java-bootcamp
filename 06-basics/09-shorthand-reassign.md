# Shorthand Re-assignment

Consider the following code:

```java

package com.jfarrowdev;

public class Main {

    public static void Main(String[] args) {

        int num1 = 2;
        int num2 = 10;

        System.out.println(++num1); // 3
        System.out.println(num2++); // 10
        System.out.println(num2); // 11

        num2+= 2; // 13
    }
}
```

The `+=` operator allows you to **increment and assign the value** in the same
step. You can use this with any of the arithmetic operators.
