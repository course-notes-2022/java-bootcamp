# Pass By Value with Reference Types

**References** in Java are **also pass by value**, but the value passed is a
**reference**:

```java
import java.awt.*;

public class Main {
    public static void main(String[] args) {

       Point pointA = new Point(10, 10);
       Point pointB = pointA;

       pointA.x = 100;

       System.out.println(pointA.x) // 100
       System.out.println(pointB.x) // 100

       pointB = new Point(10, 90) // Breaks the reference, as pointB is now a new object in a new memory location
    }
}

```

`pointB` now **refers** to the **same memory location as `pointA`**. Any changes
to `pointA` will be reflected in `pointB`, and vice-versa.

![pass by reference](./screenshots/pass-by-reference.png)
