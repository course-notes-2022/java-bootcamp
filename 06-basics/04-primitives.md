# Primitive Data Types Difference

Primitives allow us to store simple values: whole numbers, decimal values,
characters, etc:

| Type      | Size (bits) | Minimum                 | Maximum                 |
| --------- | ----------- | ----------------------- | ----------------------- |
| `boolean` | 1           |
| `byte`    | 8           | -128                    | 127                     |
| `short`   | 16          | -32,768                 | 32,767                  |
| `char`    | 16          |                         |                         |
| `int`     | 32          | -2147483648             | 2147483647              |
| `long`    | 64          | -9223372036854775808    | 9223372036854775807     |
| `float`   | 32          | 1.40129846432481707e-45 | 3.40282346638528860e+38 |
| `double`  | 64          | 2.2250738585072014e-308 | 1.7976931348623157e+308 |
