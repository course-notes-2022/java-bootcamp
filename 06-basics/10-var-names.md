# Naming Variables

Observer the following conventions when naming variables in Java:

- Variable names should have **semantic meaning**:
  ```java
      int foo; // NO
      int goalsCount: // YES
  ```
- Use `camelCase` for variable names

- Prefix var name with 'is' or 'has' for booleans:
  ```java
      boolean isAdult = true;
      boolean hasFever = false
  ```
