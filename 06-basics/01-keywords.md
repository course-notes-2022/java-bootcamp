# Reserved Keywords

Java has several **keywords** that are reserved for use by the language itself,
for example:

- `public`: This is a reserved keyword

If you attempt to use a reserved keyword in a program (as a variable name, for
example), you will get a compilation error.
