# Reference Types and Objects

**Reference types** are used to store simple values:

```java
import java.awt.*;

public class Main {
    public static void main(String[] args) {

        // Reference types
        int age = 21;

        // Objects
        Point pointA = new Point(10, 10)

        // Objects have methods associated
        System.out.println(pointA.move(10, 11));
    }
}

```

**Objects** allow you to store more **complex** information, such as the
`pointA` object that stores a set of coordinates.
