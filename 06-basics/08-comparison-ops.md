# Comparison Operators

Java has the following comparison operators:

| Operator | Operation             |
| -------- | --------------------- |
| `<`      | less than             |
| `<=`     | less than/equal to    |
| `>`      | greater than          |
| `>=`     | greater than/equal to |
| `==`     | equality              |
| `!=`     | inequality            |

The result of all of these operators is a `boolean` value.
