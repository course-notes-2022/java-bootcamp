# Primitives, Reference Types, and Objects

There are differences between how these are stored in memory.

# Types of Memory in Java

**Stack memory** is used for static memory allocation and the execution of a
thread LIFO; Last-in-First-out.

A **stack frame** contains all the data for one function call. The stack frame
only exists during the execution time of a function, including any references.

**Heap** memory is used to store objects and JRE classes at runtime. New objects
are always created in heap memory. **References** to thesed objects are stored
in **stack memory**.

![java memory types](./screenshots/java-memory-types.png)

```java
import java.awt.*;

public class Main {
    public static void main(String[] args) {

        int age = 21; // Primitives are stored by VALUE in the Memory Stack

        // Objects are stored by REFERENCE to their location in the HEAP
        Point pointA = new Point(10, 10)

        System.out.println(pointA.move(10, 11));
    }
}

```
