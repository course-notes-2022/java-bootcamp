# Variables

**Variables** allow us to **store a value** to a location in memory.

```java
package com.jfarrowdev;

public class Main {
    public static void main(String[] args) {
        int number1 = 20;
        int number2 = 30;
        int result = number1 + number2;
        System.out.println(result);
        double pi = 3.14;
        boolean isAdult = false;
    }
}
```

Java is a **statically-typed** language, which means (in part) that variables
**must be declared with a data type**. Once declared, a variable can contain
values of that type only.

**Primitive** data types are data such as **numbers**, **booleans**, etc.
