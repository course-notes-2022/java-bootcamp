# `do/while` Loop

The `do/while` loop is similar to the `while` loop, but it guarantees the loop
to run **at least once**:

```java
public class Main { public static void main(String[] args) {

        String[] names = ["James", "Nadia", "Sophia", "Alex", "Saleh"];

        int counter = 0;

        // do/while loop
        do {
            System.out.println(names[counter]);
            counter++;
        } while (counter < names.length);
    }

}
```
