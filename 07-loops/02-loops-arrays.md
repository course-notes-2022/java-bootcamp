# Loops and Arrays

```java
package com.jfarrowdev;

public class Main {
    public static void main(String[] args) {

        String[] names = ["James", "Nadia", "Sophia", "Alex", "Saleh"];

        for(int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }
    }
}
```
