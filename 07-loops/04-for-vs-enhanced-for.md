# `for i` vs. Enhanced `for` Loop

Use a `for i` loop when you want to have access to the **index** (`i`). Use the
enhanced `for` when you don't need access to the index.
