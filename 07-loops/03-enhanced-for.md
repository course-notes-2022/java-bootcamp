# Enhanced `for` Loop

The **enhanced `for` loop** in Java provides a shorter syntactical alternative
to the `for` loop:

```java
package com.jfarrowdev;

public class Main {
    public static void main(String[] args) {

        String[] names = ["James", "Nadia", "Sophia", "Alex", "Saleh"];

        // Enhanced for loop
        for (String n : names) {
            System.out.println(n);
        }
    }
}
```
