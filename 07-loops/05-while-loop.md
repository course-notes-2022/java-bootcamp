# `while` Loop

There are two essential parts of a while loop:

- A **boolean** expression (that determines when to run the loop)
- An **update** to the expression (that determines when to **stop** the loop)

```java
public class Main { public static void main(String[] args) {

        String[] names = ["James", "Nadia", "Sophia", "Alex", "Saleh"];

        int counter = 0;

        // while loops
        while(counter < names.length) {
            System.out.println(names[counter]);
            counter++;
        }
    }

}
```
