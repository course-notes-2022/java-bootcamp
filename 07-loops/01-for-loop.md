# `for` Loops

**Loops** allow us to repeat sections of code multiple times.

```java
package com.jfarrowdev;

public class Main {
    public static void main(String[] args) {

        System.out.println("Start of loop");

        // `for` loop syntax
        for(int i = 0; i <= 10; i++) {
            System.out.println(i);
        }

        System.out.println("End of loop");

        // Use the increment/decrement operator to run the loop in increments
        for(int j = 100; j >=0 j-= 10) {
            System.out.println("j")
        }
    }
}
```
