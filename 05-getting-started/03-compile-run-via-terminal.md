# Compiling and Running Java Programs via Terminal

We can run our programs without a GUI by using the terminal:

- `javac com/jfarrowdev/Main.java`: **Compile** the program into **bytecode**.
  Note the `Main.class` file that is created. Also note the bytecode file(s)
  created in the `out/production/{project-name}` directory.

- `java com.jfarrowdev.Main`: **Run** the code using the JVM. Note that the
  `.java` extension is **not** used.
