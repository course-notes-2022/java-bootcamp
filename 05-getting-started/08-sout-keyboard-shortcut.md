# `SOUT` Shortcut

We can use the following shortcut for `System.out.println`:

- `sout`: Type this in IntelliJ, and the IDE will auto-substitute the full
  command.
