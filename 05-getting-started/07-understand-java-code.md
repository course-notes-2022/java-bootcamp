# Understanding Java Code

In this lesson we'll look at Java **syntax**, and how it works:

```java
package com.jfarrowdev; // Every line that is not a method or class MUST end with a ;

public class Main { // Classes enclosed in {}
    public static void main(String[] args) {
        // write your code here
        System.out.println("Hello Java!");
        System.out.println(10 + 10)
    }
}
```
