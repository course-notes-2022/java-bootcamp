# The `main` Method

The `main` method is a special method that is the **main entry point** for your
Java program. If you _don't have_ a `main` method, **your program will not
run**. The `Main` _class_ can be renamed as you like, but you _must_ have a
`main` method.
