# Your First Java Program

Right now, we have a basic boilerplate in our `Main` class. Let's modify it a
bit to create a simple first program:

```java
package com.jfarrowdev;

public class Main {
    public static void main(String[] args) {
        // write your code here
        System.out.println("Hello Java!");
        System.out.println(10 + 10)
    }
}
```

The `System.out` command allows us to print to the **console**.
