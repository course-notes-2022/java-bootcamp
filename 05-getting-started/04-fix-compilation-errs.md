# Fixing Compilation Errors

You **must** fix any compilation errors before you can compile your source code.
Pay attention to your IDE display, as it will usually alert you to any
compilation errors that exist. **Note that the errors may not necessarily exist
in your `Main` class file**, but they will still cause compilation errors!
