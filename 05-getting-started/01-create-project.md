# Create a Project

1. In IntelliJ IDE: File > New > Project > Create
2. Add project name, JDK, etc.
3. Create

## What Does our Project Contain?

- `.idea`: XML configuration files for IntelliJ
- `src`: Directory containing our application source code

## Create a New Package

Inside the `src` directory:

1. Create a new **package** with your domain name of choice.

2. Move the `Main` class into your new package. Note that the package name
   appears as a `package` declaration at the top of the `Main` class.
