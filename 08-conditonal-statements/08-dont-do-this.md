# Don't Do This with `if` Statements!

```java

public class Main() {

    public static void main(String[] args) {
        int age = 15;
        boolean isToddler = age <= 3 && age >= 1;
        boolean isAdult = age >= 18;

        if(isAdult == false) { // NO; this is REDUNDANT, because isAdult already evaluates to a boolean value!
           //...
        }
    }
}
```
