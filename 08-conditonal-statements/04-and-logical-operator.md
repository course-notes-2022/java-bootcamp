# `&&` Logical Operator

**Logical operators** are used to combine two or more comparison operators.

The `&&` operator is the logical "AND" operator. It evaluates to `true` if
**all** expressions on both sides evaluate to `true`, and `false` otherwise.

```java

public class Main() {

    public static void main(String[] args) {
        int age = 15;
        boolean isToddler = age <= 3 && age >= 1; // Note the use of the `&&` logical operator
        boolean isAdult = age >= 18;

        if(isAdult) {
            System.out.println("is an adult");
        } else if(isToddler) {
            System.out.println("is a toddler");
        }
        else {
            System.out.println("is not an adult");
        }
    }
}
```
