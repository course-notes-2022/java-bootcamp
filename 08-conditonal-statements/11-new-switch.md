# New `switch` Statement

In the latest version of Java (since Java 14), we have `switch expressions`
available to us. Switch expressions allow us to **store the result** of a switch
statement in a **variable**:

```java
public class Main {
    public static void main(String[] args) {
        char grade = 'A';
        String result = switch(grade) {
            case 'A' -> "Excellent";
            case 'B','C' -> "Pass";
            default -> "Fail";
    }
}
```
