# `if` Statements with Conditions

```java

public class Main() {

    public static void main(String[] args) {
        int age = 15;
        boolean isAdult = age >= 18;

        if(isAdult) {
            System.out.println("is an adult");
        } else {
            System.out.println("is not an adult");
        }
    }
}
```
