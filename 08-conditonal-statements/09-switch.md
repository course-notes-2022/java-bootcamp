# Switch Statement

The `switch` statement allows us to handle conditional logic without using
`if-else` statements. It improves the readability when there are multiple
conditions:

```java

public class Main {
    public static void main(String[] args) {
        // without switch
        char grade = 'A';
        if(grade == 'A'){
            System.out.println("Excellent");
        } else if (grade == 'B' || grade == 'C') {
            System.out.println("Pass");
        } else {
            System.out.println("Fail");
        }
    }
}
```

```java
public class Main {
    public static void main(String[] args) {
        // switch
        char grade = 'A';
        switch(grade) {
            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
            case 'C':
                System.out.println("Pass");
                break;
            default:
                System.out.println("Fail");
    }
}
```
