# `else if`

```java

public class Main() {

    public static void main(String[] args) {
        int age = 15;
        boolean isToddlger = age <= 3 && age >= 1;
        boolean isAdult = age >= 18;

        if(isAdult) {
            System.out.println("is an adult");
        } else if(isToddler) {
            System.out.println("is a toddler");
        }
        else {
            System.out.println("is not an adult");
        }
    }
}
```
