# `or` Logical Operator

The `||` ('or') logical operator evaluates to `true` if **either** expression on
the left or right of the operator evaluates to `true`, or `false` if **both**
expressions evaluate to `false`:

```java

public class Main() {

    public static void main(String[] args) {

       String gender = "FEMALE";

       if(gender.equalsIgnoreCase("FEMALE") || gender.equalsIgnoreCase("MALE")) {
        System.out.println("Valid gender.");
       } else {
        System.out.println("Invalid gender.");
       }
    }
}
```
