# `!` on `if` Statements

The `!` operator **negates** whatever boolean expression comes after it:

```java

public class Main() {

    public static void main(String[] args) {
        int age = 15;
        boolean isAdult = age >= 18;

        if(!isAdult) {
            System.out.println("is NOT an adult");
        }
        else {
            System.out.println("is an adult");
        }
    }
}
```
