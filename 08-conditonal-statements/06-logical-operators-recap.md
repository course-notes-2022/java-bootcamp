# Logical Operators Recap

| Conditions       | Evaluates To |
| ---------------- | ------------ |
| true && true     | true         |
| true && false    | false        |
| false && false   | false        |
| true \|\| false  | true         |
| false \|\| false | false        |
| false \|\| true  | true         |
| true \|\| true   | true         |

Note that you can also **group** logical operators:

```java
true && (false || true)
```
