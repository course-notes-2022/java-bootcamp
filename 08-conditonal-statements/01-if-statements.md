# Conditional Statements

**Conditional statements** allow us to execute code **optionally**, depending on
whether a certain condition evaluates to `true` or `false`.

```java

public class Main() {

    public static void main(String[] args) {

        if(true) {
            System.out.println("This code runs when true");
        } else {
            System.out.println("This code runs when false");
        }
        System.out.println("this code will always run...");
    }
}
```
