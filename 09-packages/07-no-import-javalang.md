# No Import for `java.lang`

The `java.lang` package is the **default** package in Java, so there is no need
to explicitly import it:

```java
package com.jfarrowdev;

public class Main {
    public static void main() {
        String str1 = "hello"; // No need to import classes from the 'java.lang' package
        double pi = Math.PI;
    }
}
```
