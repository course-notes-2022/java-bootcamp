# Creating Packages

To create a package:

- The package must live in the `src` folder.
- The package must have a "root" folder. Typically packages are structured in
  the following way:

  ```
  |-src
  |--/com
  |----/yourdomain
  |------/yourpackage
  ```

  The package declaration in all files in the `yourpackage` package would then
  look like:

  ```java
    package com.yourdomain.yourpackage
  ```
