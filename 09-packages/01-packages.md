# Packages

**Packages** are simply folders that allow you to structure a Java
application/project. You can create packages in IntelliJ using the File > New >
Package selection.

The first statement in a Java file is a **package declaration** that declares
which package the file belongs to.
