# Import with Fully-Qualified Name

You should import and use classes using their **fully-qualified name**, to avoid
collisions with package names:

```java
package com.jfarrowdev;

import java.util.Date;
import java.sql.Date;

public class Main {
    public static void main() {
        java.util.Date date = new java.util.Date();
        java.sql.Date nutherDate = new java.sql.Date();
    }
}
```
