# Optimize Imports

You can remove unused imports in IntelliJ using the `CTRL + ALT + o` keyboard
shortcut.
