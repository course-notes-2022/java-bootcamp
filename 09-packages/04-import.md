# `import` Keyword

How do we use classes from other packages in our programs? With the `import`
keyword:

```java
package com.jfarrowdev;

import java.awt.*; // Import all classes from the java.awt package

```

We can also import specific classes (better practice):

```java
package com.jfarrowdev;

import java.awt.Point; // Import Point class from the java.awt package
```
