package com.jfarrowdev.exercises;

public class IfExercise {

    private static void printExNumber(int num) {
        System.out.println("IfExercise ex" + num);
    }
    private static void printSeparator() {
        System.out.println("---------------------------");
    }
    // Exercise 1
    public static void ex1(int x, int y) {
        printExNumber(1);
        if(x == y) {
            System.out.println("Equal");
        }
        printSeparator();
    }

    // Exercise 2
    public static void ex2() {
        printExNumber(2);
        String strA = "The rain in Spain falls mainly on the plain";
        String strB = "The rain in Spain falls mainly on the plain";

        if(strA == strB) {
            System.out.println("Equal");
        }
        printSeparator();
    }

    // Exercise 3
    public static void ex3() {
        printExNumber(3);
        String strA = "The rain in Spain falls mainly on the plain";
        String strB = new String(strA);
        if(strA == strB) {
            System.out.println("Equal");
        } else {
            System.out.println("Not equal");
        }
        printSeparator();
    }

    // Exercise 4
    public static void ex4(int x, int y) {
        printExNumber(4);
        if(x > y) {
            System.out.println("x is greater than y");
        } else {
            System.out.println("x is not greater than y");
        }
        printSeparator();
    }

    // Exercise 5
    public static void ex5(int x, int y, int z) {
        printExNumber(5);
        if((x > y) && (x < z)) {
            System.out.println("x is > y and < z");
        } else {
            System.out.println("condition is not met");
        }
        printSeparator();
    }

    // Exercise 6
    public static void ex6(int x, int y, int z) {
        printExNumber(6);
        if((x > y) || (x < z)) {
            System.out.println("x is > y or < z");
        } else {
            System.out.println("condition is not met");
        }
        printSeparator();
    }

    // Exercise 7
    public static void ex7() {
        printExNumber(7);
        String strA = "The rain in Spain falls mainly on the plain";
        if(strA.charAt(0) == 'z') {
            System.out.println("The first character of '" + strA + "' is z");
        } else {
            System.out.println("The first character of '" + strA + "' is NOT z");
        }
        printSeparator();
    }

    // Exercise 8
    public static void ex8(String str, char c) {
        printExNumber(8);
        boolean found = false;
        for(int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == c && !found) {
                System.out.println("Character at index " + i + " is " + c);
                found = true;
                break;
            } else {
                System.out.println("Character at index " + i + " is NOT " + c);
            }
        }
        printSeparator();
    }

    // Exercise 9
    public static void ex9() {
        printExNumber(9);
        for(int i = 0; i <= 20; i++) {
            if(i % 2 == 0) {
                System.out.println(i);
            }
        }
        printSeparator();
    }
}
