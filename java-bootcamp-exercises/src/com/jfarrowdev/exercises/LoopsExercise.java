package com.jfarrowdev.exercises;

import java.util.Arrays;

public class LoopsExercise {

    public static void exercise1() {
        int[] arr = new int[3];
        System.out.println("exercise1 arr:" + Arrays.toString(arr));
        System.out.println("-------------------------");
    }

    public static void exercise2() {
        int[] arr = new int[3];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = 4;
        }
        System.out.println("exercise2 arr:" + Arrays.toString(arr));
        System.out.println("-------------------------");
    }

    public static void exercise3() {
        int[] arr = new int[3];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = 4;
        }
        arr[1] = 17;
        System.out.println("exercise2 arr:" + Arrays.toString(arr));
        System.out.println("-------------------------");
    }

    public static void exercise5() {
        System.out.println("exercise 5");
        String[] arr = {"a", "b", "c", "d"};
        System.out.println(Arrays.toString(arr));
        System.out.println("-------------------------");
    }
    public static void exercise6() {
        System.out.println("exercise 6");
        String[] arr = {"a", "b", "c", "d"};
        String[] arr2 = arr;
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));
        arr2[0] = "z";
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));
        System.out.println("-------------------------");
    }

    public static void exercise7() {
        System.out.println("exercise 7");
        String[] arr = {"a", "b", "c", "d"};
        String[] arr2 = Arrays.copyOf(arr,arr.length);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));
        arr2[0] = "z";
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));
        System.out.println("-------------------------");
    }

    public static void exercise8() {
        System.out.println("exercise 8");
        for(int i = 0; i <= 10; i++) {
            System.out.println(i);
        }
        System.out.println("-------------------------");
    }

    public static void exercise9() {
        System.out.println("exercise 9");
        for(int i = 10; i >= 0; i--) {
            System.out.println(i);
        }
        System.out.println("-------------------------");
    }

    public static void exercise10() {
        System.out.println("exercise 10");
        int[] result = new int[10];
        for(int i = 1; i < result.length ; i++) {
            result[i] = i;
        }
        System.out.println(Arrays.toString(result));
        System.out.println("-------------------------");
    }

    public static void exercise11() {
        System.out.println("exercise 11");
        int[] nums = {1,3,5,7,9,11};
        int sum = 0;
        for(int n : nums) {
            sum+= n;
        }
        System.out.println(sum);
        System.out.println("-------------------------");
    }

    public static void exercise12() {
        System.out.println("exercise 12");
        String[] arr = {"i", "sure", "do", "love", "bees"};
        for(int i = 0; i < arr.length; i++) {
            arr[i] = arr[i].toUpperCase();
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("-------------------------");
    }

    public static void exercise13() {
        System.out.println("exercise 13");
        String[] arr = {"i", "sure", "do", "love", "bees"};
        for(int i = 0; i < arr.length; i++) {
            arr[i] = arr[i].substring(0, 1).toUpperCase() + arr[i].substring(1, arr[i].length());
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("-------------------------");
    }
}
