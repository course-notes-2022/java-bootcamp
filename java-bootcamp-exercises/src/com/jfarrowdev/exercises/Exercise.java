package com.jfarrowdev.exercises;

import com.jfarrowdev.email.EmailValidator;

public class Exercise {
    public static void main(String[] args) {
        checkEmail();
        // runBasicsExercises();
        // runLoopsExercises();
        // runIfExercises();

    }

    public static void checkEmail() {
        System.out.println(EmailValidator.validateEmail("foojenkins@bar.com"));
        System.out.println(EmailValidator.validateEmail("notavalidemail.com"));
    }
    public static void runBasicsExercises() {
        // Exercise 1
        System.out.println("I can compile and run from the terminal. Hooray!");

        // Exercise 2
        System.out.println("I love cats");
        // System.out.println("I hate cats");

        // Exercise 3
        float numA = 22.1f;
        float numB = 9.999f;
        boolean result = numA <= numB;
        System.out.println("The smallest result is: " + result);

        // Exercise 4
        String strA = "hello world";
        String strB = "dlrow olleH";
        boolean resultB = strA.length() == strB.length();
        System.out.println("Result b: " + resultB);

        // Exercise 5
        System.out.println(strA.toUpperCase());

        // Exercise 6
        System.out.println(strA.substring(0,1).toUpperCase() + strA.substring(1, strA.length()));
    }
    public static void runLoopsExercises() {
        LoopsExercise.exercise1();
        LoopsExercise.exercise2();
        LoopsExercise.exercise3();
        LoopsExercise.exercise5();
        LoopsExercise.exercise6();
        LoopsExercise.exercise7();
        LoopsExercise.exercise8();
        LoopsExercise.exercise9();
        LoopsExercise.exercise10();
        LoopsExercise.exercise11();
        LoopsExercise.exercise12();
        LoopsExercise.exercise13();
    }

    public static void runIfExercises() {
        IfExercise.ex1(11, 22);
        IfExercise.ex2();
        IfExercise.ex3();
        IfExercise.ex4(22, 11);
        IfExercise.ex5(10, 20, 30);
        IfExercise.ex6(10, 20, 30);
        IfExercise.ex7();
        IfExercise.ex8("The rain in Spain falls mainly on the plain", 'a');
        IfExercise.ex9();
    }
}
